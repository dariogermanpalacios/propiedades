--CREATE DATABASE CRUDChallenge

USE CRUDChallenge

CREATE TABLE Propiedades (

		 --ID							INT PRIMARY KEY IDENTITY
		 ID INT IDENTITY(1,1) PRIMARY KEY
		,[Tipo de Propiedad]		NVARCHAR(100)
		,[Tipo de Operacion]		NVARCHAR(100)
		,[Descripcion]				NVARCHAR(MAX)
		,[Ambientes]				TINYINT
		,[M2]						DECIMAL(10,2)
		,[Antiguedad]				SMALLINT
		,[Coordenadas de ubicacion]	NVARCHAR(100)
		,[Lista de imagenes]		NVARCHAR(MAX)
);

INSERT INTO Propiedades ([Tipo de Propiedad], [Tipo de Operacion], [Descripcion], [Ambientes], [M2], [Antiguedad], [Coordenadas de ubicacion], [Lista de imagenes])
VALUES 
('Casa', 'Venta', 'Hermosa casa en el campo con vistas panor�micas.', 4, 250.50, 10, '40.7128� N, 74.0060� W', 'imagen1.jpg, imagen2.jpg'),
('Departamento', 'Venta', 'Departamento c�ntrico con excelente ubicaci�n.', 2, 80.75, 5, '34.0522� N, 118.2437� W', 'imagen3.jpg, imagen4.jpg'),
('Casa', 'Alquiler', 'Amplia casa familiar con jard�n y piscina.', 5, 350.25, 15, '51.5074� N, 0.1278� W', 'imagen5.jpg, imagen6.jpg'),
('Terreno', 'Venta', 'Terreno ideal para construcci�n de vivienda.', NULL, 500.00, NULL, '25.7617� N, 80.1918� W', 'imagen7.jpg'),
('Local Comercial', 'Alquiler', 'Local comercial en zona transitada.', NULL, 120.00, 8, '48.8566� N, 2.3522� E', 'imagen8.jpg, imagen9.jpg'),
('Departamento', 'Alquiler', 'Departamento amoblado en �rea residencial.', 3, 100.00, 3, '33.6844� S, 65.5010� W', 'imagen10.jpg'),
('Casa', 'Venta', 'Casa colonial restaurada en el casco hist�rico.', 6, 400.75, 50, '19.4326� N, 99.1332� W', 'imagen11.jpg, imagen12.jpg'),
('Oficina', 'Alquiler', 'Oficina en edificio corporativo con seguridad 24/7.', NULL, 200.50, 10, '40.7128� N, 74.0060� W', 'imagen13.jpg'),
('Local Comercial', 'Venta', 'Local comercial con amplio escaparate.', NULL, 150.00, 5, '51.5074� N, 0.1278� W', 'imagen14.jpg'),
('Terreno', 'Venta', 'Terreno en desarrollo urban�stico con servicios b�sicos.', NULL, 800.00, NULL, '34.0522� N, 118.2437� W', 'imagen15.jpg');


--select * from Propiedades