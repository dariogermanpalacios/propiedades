﻿using System;

namespace Propiedades.Dtos
{
    public class InformacionPropiedadesDto
    {
        public int Id { get; set; }
        public string TipoDePropiedad { get; set; }
        public string TipoDeOperacion { get; set; }
        public string Descripcion { get; set; }
        public byte? Ambientes { get; set; }
        public decimal? M2 { get; set; }
        public short? Antiguedad { get; set; }
        public string CoordenadasDeUbicacion { get; set; }
        public string ListaDeImagenes { get; set; }
    }
}
