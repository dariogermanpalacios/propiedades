﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace Propiedades.Models
{
    public partial class CRUDChallengeContext : DbContext
    {
        public CRUDChallengeContext()
        {
        }

        public CRUDChallengeContext(DbContextOptions<CRUDChallengeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Propiedade> Propiedades { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("Database"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Propiedade>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CoordenadasDeUbicacion)
                    .HasMaxLength(100)
                    .HasColumnName("Coordenadas de ubicacion");

                entity.Property(e => e.ListaDeImagenes).HasColumnName("Lista de imagenes");

                entity.Property(e => e.M2).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TipoDeOperacion)
                    .HasMaxLength(100)
                    .HasColumnName("Tipo de Operacion");

                entity.Property(e => e.TipoDePropiedad)
                    .HasMaxLength(100)
                    .HasColumnName("Tipo de Propiedad");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
