﻿using Propiedades.Models;
using Propiedades.Dtos;
using System.Collections.Generic;

namespace Propiedades.Services.Interfaces
{
    public interface IPropiedadesService
    {
        Respuesta<IEnumerable<InformacionPropiedadesDto>> ObtenerDatos();
        Respuesta<object> AgregarPropiedad(InformacionPropiedadesDto propiedadDto);
        Respuesta<object> ActualizarPropiedad(InformacionPropiedadesDto propiedadDto);
        Respuesta<object> EliminarPropiedad(int id);
    }
}
