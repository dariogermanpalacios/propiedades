﻿using Propiedades.Dtos;
using Propiedades.Models;
using Propiedades.Repositories.Interfaces;
using Propiedades.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace Propiedades.Services
{
    public class PropiedadesService : IPropiedadesService
    {
        private readonly IPropiedadesRepository _propiedadesRepository;
        private readonly ILogger<PropiedadesService> _logger;

        public PropiedadesService(IPropiedadesRepository propiedadesRepository, ILogger<PropiedadesService> logger)
        {
            _propiedadesRepository = propiedadesRepository;
            _logger = logger;
        }

        /// <summary>
        /// Obtiene datos de Propiedades
        /// </summary>
        /// <returns>Datos de Propiedades</returns>
        public Respuesta<IEnumerable<InformacionPropiedadesDto>> ObtenerDatos()
        {
            var respuesta = new Respuesta<IEnumerable<InformacionPropiedadesDto>>();

            try
            {
                _logger.LogInformation("ObtenerDatos - Obteniendo datos de propiedades");
                var propiedades = _propiedadesRepository.ObtenerDatos();
                respuesta.Data = propiedades;
                respuesta.Exitoso = true;
                respuesta.Mensaje = "Datos obtenidos exitosamente.";
            }
            catch (Exception ex)
            {
                _logger.LogError("ObtenerDatos - Ocurrió un error al obtener los datos de propiedades: {ex}", ex);
                respuesta.Exitoso = false;
                respuesta.Mensaje = "Ocurrió un error al obtener los datos de propiedades.";
            }

            return respuesta;
        }

        /// <summary>
        /// Agrega una nueva propiedad a la base de datos
        /// </summary>
        /// <param name="propiedadDto">Propiedad a agregar</param>
        /// <returns>Objeto con información</returns>
        public Respuesta<object> AgregarPropiedad(InformacionPropiedadesDto propiedadDto)
        {
            var respuesta = new Respuesta<object>();

            try
            {
                _logger.LogInformation("PropiedadesService - Agregando nueva propiedad");
                _propiedadesRepository.AgregarPropiedad(propiedadDto);

                respuesta.Data = propiedadDto;
                respuesta.Exitoso = true;
                respuesta.Mensaje = "Propiedad agregada exitosamente.";
            }
            catch (Exception ex)
            {
                _logger.LogError("PropiedadesService - Ocurrió un error: {@ex}", ex);
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        /// <summary>
        /// Actualiza la información de una propiedad en la base de datos
        /// </summary>
        /// <param name="propiedadDto">Datos de la propiedad a actualizar</param>
        /// <returns>Objeto con información</returns>
        public Respuesta<object> ActualizarPropiedad(InformacionPropiedadesDto propiedadDto)
        {
            var respuesta = new Respuesta<object>();

            try
            {
                _logger.LogInformation("PropiedadesService - Actualizando propiedad");
                _propiedadesRepository.ActualizarPropiedad(propiedadDto);
                respuesta.Exitoso = true;
                respuesta.Mensaje = "Propiedad actualizada exitosamente.";
            }
            catch (Exception ex)
            {
                _logger.LogError("PropiedadesService - Ocurrió un error: {@ex}", ex);
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        /// <summary>
        /// Elimina una propiedad de la base de datos
        /// </summary>
        /// <param name="id">ID de la propiedad a eliminar</param>
        /// <returns>Objeto con información</returns>
        public Respuesta<object> EliminarPropiedad(int id)
        {
            var respuesta = new Respuesta<object>();

            try
            {
                _logger.LogInformation("PropiedadesService - Eliminando propiedad");
                _propiedadesRepository.EliminarPropiedad(id);

                respuesta.Data = id;
                respuesta.Exitoso = true;
                respuesta.Mensaje = $"Propiedad con ID: {id} eliminada exitosamente.";
            }
            catch (Exception ex)
            {
                _logger.LogError("PropiedadesService - Ocurrió un error: {@ex}", ex);
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }
    }
}
