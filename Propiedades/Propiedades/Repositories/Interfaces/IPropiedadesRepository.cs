﻿using Propiedades.Dtos;
using Propiedades.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Propiedades.Repositories.Interfaces
{
    public interface IPropiedadesRepository
    {
        IEnumerable<InformacionPropiedadesDto> ObtenerDatos();
        void AgregarPropiedad(InformacionPropiedadesDto propiedadDto);
        void ActualizarPropiedad(InformacionPropiedadesDto propiedadDto);
        void EliminarPropiedad(int id);
    }
}
