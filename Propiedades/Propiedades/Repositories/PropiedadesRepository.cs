﻿using Propiedades.Dtos;
using Propiedades.Models;
using Propiedades.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Principal;

namespace Propiedades.Repositories
{
    public class PropiedadesRepository : IPropiedadesRepository
    {
        private readonly CRUDChallengeContext _context;

        public PropiedadesRepository(CRUDChallengeContext context)
        {
            _context = context;
        }

        public IEnumerable<InformacionPropiedadesDto> ObtenerDatos()
        {
            var datos = _context.Propiedades
                .Select(propiedades => new InformacionPropiedadesDto()
                {
                    Id = propiedades.Id,
                    TipoDePropiedad = propiedades.TipoDePropiedad,
                    TipoDeOperacion = propiedades.TipoDeOperacion,
                    Descripcion = propiedades.Descripcion,
                    Ambientes = propiedades.Ambientes,
                    M2 = propiedades.M2,
                    Antiguedad = propiedades.Antiguedad,
                    CoordenadasDeUbicacion = propiedades.CoordenadasDeUbicacion,
                    ListaDeImagenes = propiedades.ListaDeImagenes
                });

            return datos.ToList();
        }

        public void AgregarPropiedad(InformacionPropiedadesDto propiedadDto)
        {
            var nuevaPropiedad = new Propiedade
            {
                TipoDePropiedad = propiedadDto.TipoDePropiedad,
                TipoDeOperacion = propiedadDto.TipoDeOperacion,
                Descripcion = propiedadDto.Descripcion,
                Ambientes = propiedadDto.Ambientes,
                M2 = propiedadDto.M2,
                Antiguedad = propiedadDto.Antiguedad,
                CoordenadasDeUbicacion = propiedadDto.CoordenadasDeUbicacion,
                ListaDeImagenes = propiedadDto.ListaDeImagenes
            };

            _context.Propiedades.Add(nuevaPropiedad);
            _context.SaveChanges();
        }

        public void ActualizarPropiedad(InformacionPropiedadesDto propiedadDto)
        {
            var propiedadExistente = _context.Propiedades.Find(propiedadDto.Id);

            if (propiedadExistente != null)
            {
                propiedadExistente.TipoDePropiedad = propiedadDto.TipoDePropiedad;
                propiedadExistente.TipoDeOperacion = propiedadDto.TipoDeOperacion;
                propiedadExistente.Descripcion = propiedadDto.Descripcion;
                propiedadExistente.Ambientes = propiedadDto.Ambientes;
                propiedadExistente.M2 = propiedadDto.M2;
                propiedadExistente.Antiguedad = propiedadDto.Antiguedad;
                propiedadExistente.CoordenadasDeUbicacion = propiedadDto.CoordenadasDeUbicacion;
                propiedadExistente.ListaDeImagenes = propiedadDto.ListaDeImagenes;

                _context.SaveChanges();
            }
        }

        public void EliminarPropiedad(int id)
        {
            var propiedadAEliminar = _context.Propiedades.Find(id);

            if (propiedadAEliminar != null)
            {
                _context.Propiedades.Remove(propiedadAEliminar);
                _context.SaveChanges();
            }
        }
    }
}
