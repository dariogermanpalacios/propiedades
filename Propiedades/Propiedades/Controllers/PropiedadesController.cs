﻿using Propiedades.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Propiedades.Dtos;

namespace Propiedades.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PropiedadesController : ControllerBase
    {
        private readonly IPropiedadesService _propiedadesService;

        public PropiedadesController(IPropiedadesService propiedadesService)
        {
            _propiedadesService = propiedadesService;
        }

        [HttpGet("ObtenerDatos")]
        public IActionResult ObtenerDatos()
        {
            var response = _propiedadesService.ObtenerDatos();
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }
        [HttpPost("AgregarPropiedad")]
        public IActionResult AgregarPropiedad([FromBody] InformacionPropiedadesDto propiedadDto)
        {
            var response = _propiedadesService.AgregarPropiedad(propiedadDto);
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }

        [HttpPut("ActualizarPropiedad")]
        public IActionResult ActualizarPropiedad([FromBody] InformacionPropiedadesDto propiedadDto)
        {
            var response = _propiedadesService.ActualizarPropiedad(propiedadDto);
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }

        [HttpDelete("EliminarPropiedad/{id}")]
        public IActionResult EliminarPropiedad(int id)
        {
            var response = _propiedadesService.EliminarPropiedad(id);
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }
    }
}
