﻿//using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
//using Banco.Services.Interfaces;
using Propiedades.Controllers;
using Propiedades.Models;
using Propiedades.Dtos;
using Propiedades.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Propiedades_Test.Repositories
{
    public class PropiedadesControllerTest
    {
        PropiedadesController target;
        Mock<IPropiedadesService> _mockPropiedadesService;
        public PropiedadesControllerTest()
        {
            _mockPropiedadesService = new Mock<IPropiedadesService>();
            target = new PropiedadesController(_mockPropiedadesService.Object);
        }

        [Test]
        public void ObtenerDatos_DeberiaRetornarBadRequest_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var respuesta = new Respuesta<IEnumerable<InformacionPropiedadesDto>>
            {
                Exitoso = false,
                Mensaje = "Error al obtener datos"
            };
            _mockPropiedadesService.Setup(m => m.ObtenerDatos()).Returns(respuesta);

            // Act
            var resultado = target.ObtenerDatos() as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(400, resultado.StatusCode);
        }

        [Test]
        public void AgregarPropiedad_DeberiaRetornarOk_Cuando_ExitosoEsVerdadero()
        {
            // Arrange
            var propiedadDto = new InformacionPropiedadesDto();
            var respuesta = new Respuesta<object> { Exitoso = true };
            _mockPropiedadesService.Setup(m => m.AgregarPropiedad(propiedadDto)).Returns(respuesta);

            // Act
            var resultado = target.AgregarPropiedad(propiedadDto) as OkObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(200, resultado.StatusCode);
        }

        [Test]
        public void AgregarPropiedad_DeberiaRetornarBadRequest_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var propiedadDto = new InformacionPropiedadesDto();
            var respuesta = new Respuesta<object> { Exitoso = false, Mensaje = "Error al agregar la propiedad" };
            _mockPropiedadesService.Setup(m => m.AgregarPropiedad(propiedadDto)).Returns(respuesta);

            // Act
            var resultado = target.AgregarPropiedad(propiedadDto) as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(400, resultado.StatusCode);
        }

        [Test]
        public void ActualizarPropiedad_DeberiaRetornarOk_Cuando_ExitosoEsVerdadero()
        {
            // Arrange
            var propiedadDto = new InformacionPropiedadesDto();
            var respuesta = new Respuesta<object> { Exitoso = true };
            _mockPropiedadesService.Setup(m => m.ActualizarPropiedad(propiedadDto)).Returns(respuesta);

            // Act
            var resultado = target.ActualizarPropiedad(propiedadDto) as OkObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(200, resultado.StatusCode);
        }

        [Test]
        public void ActualizarPropiedad_DeberiaRetornarBadRequest_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var propiedadDto = new InformacionPropiedadesDto();
            var respuesta = new Respuesta<object> { Exitoso = false, Mensaje = "Error al actualizar la propiedad" };
            _mockPropiedadesService.Setup(m => m.ActualizarPropiedad(propiedadDto)).Returns(respuesta);

            // Act
            var resultado = target.ActualizarPropiedad(propiedadDto) as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(400, resultado.StatusCode);
        }

        [Test]
        public void EliminarPropiedad_DeberiaRetornarOk_Cuando_ExitosoEsVerdadero()
        {
            // Arrange
            var id = 1;
            var respuesta = new Respuesta<object> { Exitoso = true };
            _mockPropiedadesService.Setup(m => m.EliminarPropiedad(id)).Returns(respuesta);

            // Act
            var resultado = target.EliminarPropiedad(id) as OkObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(200, resultado.StatusCode);
        }

        [Test]
        public void EliminarPropiedad_DeberiaRetornarBadRequest_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var id = 1;
            var respuesta = new Respuesta<object> { Exitoso = false, Mensaje = "Error al eliminar la propiedad" };
            _mockPropiedadesService.Setup(m => m.EliminarPropiedad(id)).Returns(respuesta);

            // Act
            var resultado = target.EliminarPropiedad(id) as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(400, resultado.StatusCode);
        }


    }
}
