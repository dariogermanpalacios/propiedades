import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { Propiedad } from './Interfaces/propiedades';
import { PropiedadesService } from './Services/propiedades.service';

import { MatSnackBar } from '@angular/material/snack-bar';

import { MatDialog } from '@angular/material/dialog';
import { DialogAddEditComponent } from './Dialogs/dialog-add-edit/dialog-add-edit.component';
import { DialogoDeleteComponent } from './Dialogs/dialogo-delete/dialogo-delete.component';
import { ApiResponse } from './Interfaces/ApiResponse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['tipoDePropiedad', 'tipoDeOperacion', 'descripcion', 'ambientes', 'm2', 'antiguedad', 'coordenadasDeUbicacion', 'listaDeImagenes', 'Acciones'];
  dataSource = new MatTableDataSource<Propiedad>();
  constructor(
    private _propiedadesServicio: PropiedadesService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.mostrarPropiedades();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  mostrarPropiedades() { 
    this._propiedadesServicio.getList().subscribe({
      next: (response: any) => { // Usar any para recibir la respuesta del servicio
        const apiResponse = response as ApiResponse; // Castear la respuesta a ApiResponse
        console.log("Datos recibidos:", apiResponse);
        this.dataSource.data = apiResponse.data; // Acceder a 'data' en la respuesta para obtener el arreglo de propiedades
      }, 
      error: (error) => {
        console.error("Error al obtener datos:", error);
      }
    });
  }
  
  dialogoNuevaPropiedad() {
    this.dialog.open(DialogAddEditComponent, {
      disableClose: true,
      width: "300px"
    }).afterClosed().subscribe(resultado => {
      if (resultado === "creada") {
        this.mostrarPropiedades();
      }
    });
  }

  dialogoEditarPropiedad(dataPropiedad: Propiedad) {
    this.dialog.open(DialogAddEditComponent, {
      disableClose: true,
      width: "350px",
      data: dataPropiedad
    }).afterClosed().subscribe(resultado => {
      if (resultado === "editada") {
        this.mostrarPropiedades();
      }
    });
  }

  mostrarAlerta(msg: string, accion: string) {
    this._snackBar.open(msg, accion, {
      horizontalPosition: 'end',
      verticalPosition: 'top',
      duration: 3000
    });
  }

  dialogoEliminarPropiedad(dataPropiedad: Propiedad) {
    this.dialog.open(DialogoDeleteComponent, {
      disableClose: true,
      data: dataPropiedad
    }).afterClosed().subscribe(resultado => {
      if (resultado === "eliminar") {
        this._propiedadesServicio.deletePropiedad(dataPropiedad.id).subscribe({
          next: (data) => {
            this.mostrarAlerta("Propiedad fue eliminada", "Listo");
            this.mostrarPropiedades();
          }, error: (e) => { console.log(e); }
        });
      }
    });
  }
}
