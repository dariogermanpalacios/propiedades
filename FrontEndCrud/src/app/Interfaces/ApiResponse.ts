import { Propiedad } from "./propiedades";

export interface ApiResponse {
    data: Propiedad[];
    exitoso: boolean;
    mensaje: string;
  }
  