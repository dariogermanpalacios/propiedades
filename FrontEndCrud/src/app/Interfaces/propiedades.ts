export interface Propiedad {
    id: number;
    tipoDePropiedad: string;
    tipoDeOperacion: string;
    descripcion: string;
    ambientes: number;
    m2?: number;
    antiguedad: number;
    coordenadasDeUbicacion: string;
    listaDeImagenes: string;
}
