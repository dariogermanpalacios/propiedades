import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Propiedad } from '../Interfaces/propiedades';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PropiedadesService {

  private endpoint: string = environment.endPoint;
  private apiUrl: string = this.endpoint + "api/propiedades/";

  constructor(private http: HttpClient) { }

  getList(): Observable<Propiedad[]> {
    return this.http.get<Propiedad[]>(`${this.apiUrl}ObtenerDatos`);
  }

  addPropiedad(propiedad: Propiedad): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}AgregarPropiedad`, propiedad);
  }

  updatePropiedad(propiedad: Propiedad): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}ActualizarPropiedad`, propiedad);
  }

  deletePropiedad(id: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}EliminarPropiedad/${id}`);
  }
}
