import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import moment from 'moment';
import { Propiedad } from '../../Interfaces/propiedades'; // Cambiar la importación de la interfaz
import { PropiedadesService } from '../../Services/propiedades.service'; // Cambiar la importación del servicio

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: "DD/MM/YYYY"
  },
  display: {
    dateInput: "DD/MM/YYYY",
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
}

@Component({
  selector: 'app-dialog-add-edit',
  templateUrl: './dialog-add-edit.component.html',
  styleUrl: './dialog-add-edit.component.css',
  providers:[{provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS}]
})

export class DialogAddEditComponent implements OnInit {

  formPropiedad: FormGroup; // Cambiar el nombre del formulario
  tituloAccion: string = 'Nuevo';
  botonAccion: string = 'Guardar';
  listaPropiedades: Propiedad[] = []; // Cambiar el nombre de la lista de departamentos

  constructor(
    private dialogoRegerencia: MatDialogRef<DialogAddEditComponent>,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private _propiedadesServicio: PropiedadesService, // Cambiar la inyección del servicio
    @Inject(MAT_DIALOG_DATA) public dataPropiedad: Propiedad // Cambiar la inyección de datos
  ) {
    this.formPropiedad = this.fb.group({
      tipoDePropiedad: ['', Validators.required], // Modificar los campos del formulario según los campos de la entidad Propiedad
      tipoDeOperacion: ['', Validators.required],
      descripcion: ['', Validators.required],
      ambientes: [''],
      m2: [''],
      antiguedad: [''],
      coordenadasDeUbicacion: [''],
      listaDeImagenes: ['']
    })

    this._propiedadesServicio.getList().subscribe({
      next: (data) => {
        this.listaPropiedades = data;
      }, error: (e) => { }
    })
  }

  mostrarAlerta(msg: string, accion: string) {
    this._snackBar.open(msg, accion,{
      horizontalPosition:'end',
      verticalPosition:'top',
      duration: 3000
    });
  }

  addEditPropiedad() {

    console.log(this.formPropiedad.value)

    const modelo: Propiedad = {
      id: this.dataPropiedad ? this.dataPropiedad.id : 0,
      tipoDePropiedad: this.formPropiedad.value.tipoDePropiedad,
      tipoDeOperacion: this.formPropiedad.value.tipoDeOperacion,
      descripcion: this.formPropiedad.value.descripcion,
      ambientes: this.formPropiedad.value.ambientes,
      m2: this.formPropiedad.value.m2,
      antiguedad: this.formPropiedad.value.antiguedad,
      coordenadasDeUbicacion: this.formPropiedad.value.coordenadasDeUbicacion,
      listaDeImagenes: this.formPropiedad.value.listaDeImagenes
    }

    if (this.dataPropiedad === null) {
      this._propiedadesServicio.addPropiedad(modelo).subscribe({
        next: (data) => {
          this.mostrarAlerta("La propiedad fue creada", "Listo");
          this.dialogoRegerencia.close("creada")
        }, error: (e) => {
          this.mostrarAlerta("No se pudo crear", "Error");
        }
      });
    } else {
      this._propiedadesServicio.updatePropiedad(modelo).subscribe({
        next: (data) => {
          this.mostrarAlerta("La propiedad fue editada", "Listo");
          this.dialogoRegerencia.close("editada")
        }, error: (e) => {
          this.mostrarAlerta("No se pudo editar", "Error");
        }
      });
    }
    
  }
  ngOnInit(): void {
    if (this.dataPropiedad) {
      this.formPropiedad.patchValue({
        tipoDePropiedad: this.dataPropiedad.tipoDePropiedad,
        tipoDeOperacion: this.dataPropiedad.tipoDeOperacion,
        descripcion: this.dataPropiedad.descripcion,
        ambientes: this.dataPropiedad.ambientes,
        m2: this.dataPropiedad.m2,
        antiguedad: this.dataPropiedad.antiguedad,
        coordenadasDeUbicacion: this.dataPropiedad.coordenadasDeUbicacion,
        listaDeImagenes: this.dataPropiedad.listaDeImagenes
      });
  
      this.tituloAccion = "Editar";
      this.botonAccion = "Actualizar";
    }
  }
}
