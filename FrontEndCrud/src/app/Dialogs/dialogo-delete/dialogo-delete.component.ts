import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Propiedad } from '../../Interfaces/propiedades';

@Component({
  selector: 'app-dialogo-delete',
  templateUrl: './dialogo-delete.component.html',
  styleUrls: ['./dialogo-delete.component.css']
})
export class DialogoDeleteComponent {

  constructor(
    private dialogoRegerencia: MatDialogRef<DialogoDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public dataPropiedad: Propiedad
  ) {}

  confirmar_Eliminar() {
    if (this.dataPropiedad) {
      this.dialogoRegerencia.close("eliminar");
    }
  }
}
